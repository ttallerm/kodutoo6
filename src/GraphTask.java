import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.HashMap;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (25, 25);
      //g.createRandomTree(1000);
      System.out.println (g);


      // TODO!!! Your experiments here
      //13.13. (s) Kirjutada laiuti läbimisel põhinev algoritm, mis kontrollib, kas etteantud orienteerimata graaf on tsükliteta.

      //Loon järjekorra mille annan läbimise meetoidesse kaasa
      LinkedList<Vertex> queue = new LinkedList<>();
      queue.add(g.first); //Lisan kõige esimese tipu millest alates hakkame graafi läbima

      traverseMyGraph(queue);
   }

   /**
    * Meetod graafi läbimiseks laiuti ning leidmaks, kas on tsükkel
    * @param queue järjekord mille andsin kaasa kuhu lisan ja kust võtan järgmise tipu mida läbida, järjekorras FIFO
    *
    */

   private void traverseMyGraph(LinkedList<Vertex> queue) {

      HashMap<Vertex, Vertex> parents = new HashMap<Vertex, Vertex>(); //hashmap märkimaks ära milline tipp on millise tipu ülem. Selle ja läbitud tippude kontrolliga saan vaadata kas esineb tsükkel
      List<Vertex> visited = new ArrayList<>(); //lihtne arraylist jätmaks meelde millised tipud on läbitud

      outerloop:
      while (!queue.isEmpty()) {
         Vertex rootVertex = queue.getFirst(); // järjekorrast esimene tipp mis võetakse ja mida töödeltakse
         queue.removeFirst(); // eemaldan selle esimese järjekorrast
         visited.add(rootVertex); // märgin ta läbituks

         List<Vertex> children = getChildren(rootVertex.first, new ArrayList<>()); // Otsin tipu kõik alluvad
         Collections.reverse(children); //panen õigesse järjekorda

         //Kirjutan välja, et näha millised on minu lapsed (kontrolliks)
         System.out.println(format("I am %s and my children are: ", rootVertex.id));
         children.forEach(child -> System.out.println(child.id));

         // Kui alluvad ei ole läbi käidud, siis lisan järjekorda
         for(int i=0; i<children.size(); i++){
            if (!visited.contains(children.get(i))){
               queue.add(children.get(i)); //Lisan lapsed järjekorda
               parents.put( children.get(i),rootVertex); //märgin ülem tipu
               //Kui ei ole ülem ja on läbi käidud tipp, siis esineb tsükkel ja graafi läbimine jääb seisma
            }else if(visited.contains(children.get(i)) && children.get(i)!= parents.get(rootVertex)){
              System.out.println("Esineb tsykkel");
               break outerloop;
            }
         }

         //Kirjutan välja kontrolliks milline on minu ülem (kontrolliks)
         if(parents.get(rootVertex) == null){
            System.out.println("Olen root node ja mul pole parentit");
         }else{
            System.out.println("my parent is: " + parents.get(rootVertex).id);
         }
         System.out.println("-------------------------------------");
      }
   }

   // Leiab ühe punkti kõik lapsed ja tagastab nende nimekirja

   /**
    * Rekursiivne Meetod mis otsib ühe tipu kõik lapsed ja tagastab need
    * @param arc kaar millega on tipp ühendatud mille annan kaasa
    * @param children list lastest
    * @return kutsub ennast nii kaua välja kuni enam lapsi pole
    */
   private List<Vertex> getChildren(Arc arc, List<Vertex> children) {
      children.add(arc.target);

      if (arc.next == null) {
         return children;
      }

      return getChildren(arc.next, children);
   }





   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.

   }

}

